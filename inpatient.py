
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval, Or


class Inpatient(metaclass=PoolMeta):
    __name__ = 'health.inpatient'
    STATES = {
        'readonly': Or(Eval('state') == 'done', Eval('state') == 'finished')
    }
    interconsultations = fields.One2Many('health.interconsultation',
        'origin', 'Interconsultations', states=STATES)
    newborn = fields.Many2One('health.newborn', 'Newborn', states=STATES)
    # Mother Information
    membranes = fields.Selection([
        (None, ''),
        ('integras', 'Integras'),
        ('broken', 'Broken'),
        ], 'Membranes', states=STATES)
    broken_membranes_time = fields.Integer('Broken Membranes Time',
        states=STATES, help="In hours")
    anmiotic_liquid = fields.Integer('Anmiotic Liquid',
        states=STATES)
    gestational_age_by_lum = fields.Integer('Gestational Age by Lum',
        states=STATES)
    mother_patology = fields.Char('Mother Patology', states=STATES)
    childbirth_start_of_labour = fields.Selection([
        (None, ''),
        ('spontaneous', 'Spontaneous'),
        ('induced', 'Induced'),
        ], 'Childbirth Start of Labour', states=STATES)
    childbirth_finished = fields.Selection([
        (None, ''),
        ('caesarean', 'Caesarean'),
        ('spontaneous', 'Spontaneous'),
        ('forceps', 'Forceps'),
        ], 'Childbirth Start of Labour', states=STATES)
    signs_of_fetal_distress = fields.Selection([
        (None, ''),
        ('yes', 'Yes'),
        ('no', 'No'),
        ('ignored', 'Ignored'),
        ], 'Signs of Fetal Distress', states=STATES)
    vaccines = fields.Selection([
        (None, ''),
        ('yes', 'Yes'),
        ('no', 'No'),
        ], 'Vaccines', states=STATES)

    # Physical Exam
    newborn_weight = fields.Integer('Weight', help='(g) Grames', states=STATES)
    newborn_size = fields.Integer('Size', help='cm', states=STATES)
    newborn_malformations = fields.Text('Malformations', states=STATES)
    newborn_gestational_age = fields.Integer('Gestational Age', states=STATES)
    newborn_life_time_at_exam = fields.Integer('Life Time at Exam', states=STATES)
    newborn_cephalic_perimeter = fields.Integer('Cephalic Perimeter', states=STATES)
    newborn_hypertensive = fields.Boolean('Hypertensive', states=STATES)
    newborn_depressed = fields.Boolean('Depressed', states=STATES)
    newborn_saturated_rides = fields.Boolean('Saturated Rides', states=STATES)
    newborn_cephalus_hematoma = fields.Boolean('Cephalus Hematoma',
        states=STATES)
    newborn_blood_tumor = fields.Boolean('Blood Tumor', states=STATES)

    # Neurological
    newborn_state = fields.Selection([
        (None, ''),
        ('active', 'Active'),
        ('excited', 'Excited'),
        ('depressed', 'Depressed'),
        ('absent', 'Absent'),
        ('weak', 'Weak'),
        ], 'Vaccines', states=STATES)
    newborn_reflexes = fields.Selection([
        (None, ''),
        ('suction', 'Suction'),
        ('cocleo_palmar', 'Cocleo Palmar'),
        ('straightening', 'Straightening'),
        ('escatamiento', 'Escatamiento'),
        ('march', 'March'),
        ('palmar_prension', 'Palmar Prension'),
        ], 'Reflexes', states=STATES)
    newborn_upper_limbs = fields.Selection([
        (None, ''),
        ('normal', 'Normal'),
        ('anormal', 'Anormal'),
        ], 'Upper Limbs Exam',
         states=STATES)
    newborn_lower_limbs = fields.Selection([
        (None, ''),
        ('normal', 'Normal'),
        ('anormal', 'Anormal'),
        ], 'Upper Limbs Exam',
        states=STATES)
    newborn_branchial_paralysis = fields.Boolean('Branchial Paralysis',
        states=STATES)
    newborn_fractures = fields.Char('Fractures')
    newborn_ortolani_maneuver = fields.Selection([
        (None, ''),
        ('positive', 'Positive'),
        ('negative', 'Negative'),
        ('normal', 'Normal'),
        ], 'Ortolani Maneuver',
        states=STATES)
    newborn_vertebral_column = fields.Selection([
        (None, ''),
        ('positive', 'Positive'),
        ('negative', 'Negative'),
        ], 'Vertebral Column',
        states=STATES)
    newborn_muscle_tone = fields.Selection([
        (None, ''),
        ('hypotonic', 'Hypotonic'),
        ('normal', 'Normal'),
        ('hypertonic', 'Hypertonic'),
        ], 'Muscle Tone', states=STATES)
    newborn_moro_reflex = fields.Selection([
        (None, ''),
        ('complete', 'Complete'),
        ('incomplete', 'Incomplete'),
        ('absent', 'Absent'),
        ], 'Moro Reflex', states=STATES)
    newborn_eyes = fields.Selection([
        (None, ''),
        ('symmetrical', 'Symmetrical'),
        ('anormal', 'Anormal'),
        ('pupils', 'Pupils'),
        ], 'Newborn Eyes', states=STATES)
    newborn_nasal_fossae = fields.Selection([
        (None, ''),
        ('permeable', 'Permeable'),
        ('atresia', 'Atresia'),
        ('partition', 'Partition'),
        ], 'Nasal Fossae', states=STATES)
    newborn_oral_cavity = fields.Selection([
        (None, ''),
        ('normal', 'Normal'),
        ('cleft_palate', 'Cleft Palate'),
        ('cleft_lip', 'Cleft Lip'),
        ], 'Oral Cavity', states=STATES)
    newborn_cardiovascular_system = fields.Selection([
        (None, ''),
        ('normal_cardiac_sounds', 'Normal Cardiac Sounds'),
        ('anormal_cardiac_sounds', 'Anormal Cardiac Sounds'),
        ], 'Cardiovascular System', states=STATES)
    newborn_abdomen = fields.Selection([
        (None, ''),
        ('normal', 'Normal'),
        ('wall_malformation', 'Wall Malformation'),
        ], 'Abdomen', states=STATES)
    newborn_umbilical_cord = fields.Selection([
        (None, ''),
        ('arteries', 'Arteries'),
        ('vein', 'Vein'),
        ], 'Umbilical Cord', states=STATES)
    newborn_umbilical_cord_color = fields.Selection([
        (None, ''),
        ('normal', 'Normal'),
        ('dyeing', 'Dyeing'),
        ], 'Umbilical Cord Color', states=STATES)
    newborn_genitals_extern = fields.Selection([
        (None, ''),
        ('normal', 'Normal'),
        ('anormal', 'Anormal'),
        ], 'Genitals Extern', states=STATES)
    newborn_urinary_meatus = fields.Selection([
        (None, ''),
        ('normal', 'Normal'),
        ('anormal', 'Anormal'),
        ], 'Urinary Meatus', states=STATES)
    newborn_spleen = fields.Selection([
        (None, ''),
        ('normal', 'Normal'),
        ('anormal', 'Anormal'),
        ], 'Spleen', states=STATES)
    newborn_neck = fields.Selection([
        (None, ''),
        ('normal', 'Normal'),
        ('anormal', 'Anormal'),
        ], 'Neck', states=STATES)
    newborn_torax = fields.Selection([
        (None, ''),
        ('normal', 'Normal'),
        ('anormal', 'Anormal'),
        ('symmetry', 'symmetry'),
        ], 'Neck', states=STATES, help="Clavicles")
    newborn_breathing = fields.Selection([
        (None, ''),
        ('regular', 'Regular'),
        ('irregular', 'Irregular'),
        ], 'Breathing', states=STATES, help="Clavicles")
    newborn_radical_pulse = fields.Selection([
        (None, ''),
        ('normal', 'Normal'),
        ('anormal', 'Anormal'),
        ], 'Radical Pulse', states=STATES)
    newborn_femoral_pulse = fields.Selection([
        (None, ''),
        ('normal', 'Normal'),
        ('absent', 'Absent'),
        ], 'Femoral Pulse', states=STATES)
    newborn_ictericia = fields.Selection([
        (None, ''),
        ('slight', 'Slight'),
        ('absent', 'Absent'),
        ('intense', 'Intense'),
        ], 'Ictericia', states=STATES)
    newborn_skin_color = fields.Selection([
        (None, ''),
        ('pale', 'Pale'),
        ('cyanosis', 'Cyanosis'),
        ('pink', 'Pink'),
        ], 'Skin Color', states=STATES)
    meconium = fields.Boolean('Meconium', states=STATES)
    # Physical Exam Information


class Interconsultation(metaclass=PoolMeta):
    __name__ = 'health.interconsultation'

    @classmethod
    def _get_origin(cls):
        return super(Interconsultation, cls)._get_origin() + ['health.inpatient']
