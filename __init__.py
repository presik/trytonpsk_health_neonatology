
from trytond.pool import Pool
from . import pediatrics
from . import inpatient
from . import evaluation
# from . import configuration


def register():
    Pool.register(
        pediatrics.Newborn,
        inpatient.Inpatient,
        inpatient.Interconsultation,
        evaluation.PatientEvaluation,
        evaluation.EvaluationNeonatology,
        # configuration.HealthConfiguration,
        # neonatology.RoundingProcedure,
        # neonatology.PatientAmbulatoryCare,
        # neonatology.AmbulatoryCareProcedure,
        # neonatology.neonatologyNote,
        # neonatology.KindneonatologyNote,
        # inpatient.PatientNutritionControl,
        # inpatient.PatientWaterBalance,
        # inpatient.PatientWaterBalanceShift,
        module='health_neonatology', type_='model')
